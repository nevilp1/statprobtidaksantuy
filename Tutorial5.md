# Tutorial 5

## 1) Bayes Theorem

### Kejadian

* A = kejadian CD terpilih untuk diuji
    * P(A) = 0.3
* B = kejadian CD dipasarkan

### Kejadian bersyarat

* B|A = kejadian CD terpilih untuk diuji dan lulus (akan dipasarkan)
    * P(B|A) = 0.8

* B|A<sup>C</sup> = kejadian CD tidak terpilih untuk diuji dan dipasarkan
    * P(B|A<sup>C</sup>) = 1

### Soal

#### a) 

* P(AB<sup>C</sup>)

Total Probability Rule  
**P(A) = P(AB) + P(AB<sup>C</sup>)**

* maka P(AB<sup>C</sup>) = P(A) - P(AB)

**ingat**

* P(A|B) = P(BA)/P(B) 
* P(B|A) = P(AB)/P(A)

**maka**

* P(AB<sup>C</sup>)
* P(A) - P(B|A)P(A)
* 0.3 - 0.8 x 0.3
* 0.2 x 0.3
* 0.06

#### b) 

* P(B<sup>C</sup>|A) = P(AB<sup>C</sup>)/P(A)
* 0.06 / 0.3
* 0.2

#### c)

* P(B) = P(BA) + P(BA<sup>C</sup>)
>Karena yang kita cari BA yang sama dengan AB, jadi substitusikan yang ada , kita ambil P(AB)
* P(B) = P(B|A)P(A) + P(B|A<sup>C</sup>)P(A<sup>C</sup>)
* P(B) = 0.8 * 0.3 + (1 * (1 - P(A)))
* P(B) = 0.8 * 0.3 + 0.7
* P(B) = 0.94

#### d) 

Given B, calc the prob of A (udah dipasarkan, cek apa dia udah di uji)  

* P(A|B) = P(BA)/P(B)
* 0.8 * 0.3 (see c) / 0.94

> or we trace it back :)

* P(BA) = P(B|A)P(A)
* P(A|B) = P(B|A)P(A) / P(B)
* P(A|B) = 0.24 / 0.94


## 2) Independent Event

### Kejadian

* S = { 1,2,3,4,5,6 }
* A = Kejadian mata dadu genap
> A = { 2,4,6 }
>> P(A) = 3/6
* B = Kejadian mata dadu <= 4
> B = { 1,2,3,4 }
>> P(B) = 4/6
* P(AB) = P(A) x P(B)

Dari himpunan, dapat kita lihat bahwa P(A<sup>C</sup>B) = 2/6

* P(A<sup>C</sup>B) = P(A<sup>C</sup>) x P(B)
* P(A<sup>C</sup>B)=  3/6 x 4/6 = 2/6

Karena hasilnya sama, maka kejadian ini independen

## 3) Random Variable

X = **nHead** - **nTail** sebanyak 5 kali pelemparan

| Kepala | Ekor | X  |
|--------|------|----|
| 5      | 0    | 5  |
| 4      | 1    | 3  |
| 3      | 2    | 1  |
| 2      | 3    | -1 |
| 1      | 4    | -3 |
| 0      | 5    | -5 |

##### PMF
* PMF = P(Xi) = P(X=i)

* P(X = 5 atau X = -5) = HHHHH
> 1/2 x 1/2 x 1/2 x 1/2 x 1/2   
> 1/32

* P(X = 3 atau X = -3) = HHHHT and its permutations
> 1/32 x 5!/4! = 5/32

* P(X = 1 atau X = -1) = HHHTT
> 10/32

#### CDF
* CDF = F(Xi) = sum of P(X<) to P(Xi)

* 0 , x < -5
* 1/32 , -5 < x  < -3
* 6/32 , -3 < x < -1
* 16/32 , -1 < x < 1
* ... dst ...
* 1 , x > 5
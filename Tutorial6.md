# Tutorial 6

## Joint P(X,Y) and F(X,Y)

### a) Joint PMF

| P(X,Y) | Y=1  | Y=2  | Y=3  |
|--------|------|------|------|
| X=1    | 3/10 | 1/5  | 1/10 |
| X=2    | 1/5  | 1/10 | 0    |
| X=3    | 1/10 | 0    | 0    |

### b) F<sub>XY</sub>(2,3)
* 3/10 + 1/5  + 1/10 + 1/5 + 1/10
* 9/10

### c) F<sub>Y</sub>(3)
* 3/10 + 1/5  + 1/10 + 1/5 + 1/10 + 1/10
* 10/10

## Indikator

#### Pake indikator kalau banyak eventnya dengan random variable yang sama

* I = 1 jika sepasang hidup
* I = 0 jika ada yang mati
* P(ada yang hidup) = 3/4
* P(semua mati) = 1/4
    > LL , LD, DL, DD 

* P(Xi) = 3/4 x 3/4
* P(Xi) = 9/16

* E[Xi] = sigma XP(X)
* 1 P(Xi=1) + 0 P(Xi=0)
* P(Xi=1) = 9/16

* E[X] = sigma E[Xi]
* 100 x 9/16
* 56.25 = **57**

## Var, Covar, PMF

### a) PMF X and PMF Y 

#### PMF(X)
* **PMF(X=1)**
    * 1/8 + 1/16 + 3/16 + 1/8
    * 1/2
* **PMF(X=2)**
    * 1/16 + 1/16 + 1/8 + 1/4
    * 1/2

#### PMF(Y)
* **PMF(Y=0)**
    * 1/8 + 1/16
    * 3/16
* **PMF(Y=1)**
    * 1/16 + 1/16
    * 1/8
* **PMF(Y=2)**
    * 3/16 + 1/8
    * 5/16
* **PMF(Y=3)**
    * 1/8 + 1/4
    * 3/8

### b) E[X] and E[Y] (and E[XY])

* **E[X]** = sigma Xi P(X=i)
    * 1 x P(X=1) + 2 x P(X=2)
    * 1/2 + 1
    * 1.5 == 24/16

* **E[Y]** = sigma Yi P(Y=i)
    * 0 x P(Y=0) + 1 x P(Y=1) + 2 x P(Y=2) + 3 x P(Y=3)
    * 0 + 1/8 + 10/16 + 9/8
    * 2.5 == 30/16

* **E[XY]** = sigma sigma Xi Yj P(X=i, Y=j)
    * (1 x 0 x 1/8 + 1 x 1 x 1/16 + 1 x 2 x 3/16 + 1 x 3 x 1/8) + (2 x 0 x 1/16 + 2 x 1 x 1/16 + 2 x 2 x 1/8 + 2 x 3 x 1/4)
    * (0 + 1/16 + 6/16 + 6/16) + (0 + 2/16 + 8/16 + 24/16)
    * 47/16

### c) Var

* **Var(X)**
* E[X<sup>2</sup>] - E[X]<sup>2</sup>
* (1^2 P(X=1) + 2^2 P(X=2)) - (1 P(X=1) + 2 P(X=2))
* (1 x 1/2 + 4 x 1/2) - (1 x 1/2 + 2 x 1/2)<sup>2</sup>
* 2.5 - 2.25
* 0.25

### d) Covar(X,Y)

* **Cov(X,Y)**
* E[XY] - E[X]E[Y]
* 47/16 - 24/16 x 30/16
* (752 - 720) / 256
* -0.125
# 1 : 5 merah, 5 hijau

## a) Sample Space dari pengambilan 5 kelereng

* 10 C 5

## b) 5 Pengambilan sekaligus, 3 merah, 2 hijau

* 5C3 x 5C2 / 10C5
* merah x hijau / pengambilan-5-kelereng

## c) 3 merah, 2 hijau : selang-seling

* 3 x 2 x 2 x 1 x 1
* merah x hijau x merah x hijau x merah

## d) Pengambilan 5 kelereng satu satu tanpa pengembalian

* 10! / 5! == (10P5)

# 2 : Taruhan

P(A) = peluang ada 2 org yg ultah nya sama  
P(A<sup>c</sup>) = peluang ga ada yg ultah nya sama  
P(A) = 1 - P(A<sup>c</sup>)  
P(A<sup>c</sup>) = (365!/(365-25)!) / 365^25 = 0.57  
Jadi tidak diambil :)

# 3 : Penghasilan Suami-Istri

## a) 
* P(I) = P(IS) + P(IS<sup>c</sup>) = 18%

## b) 
* P(S|I) = P(SI) / P(I) = 49 / (49+23)

## c) 
* P(S|I<sup>c</sup>) = P(SI<sup>c</sup>)/ P(I<sup>c</sup>) = 200/400 / 1-P(I) = 25/41

# 4 : Dadu

## a)

* P(B|A<sup>c</sup>) = P(BA<sup>c</sup>) / P(A<sup>c</sup>)  
= 2/6 / 3/6  
= 2/3  

## b)

* P(A<sup>c</sup>|B<sup>c</sup>)  
= P(A<sup>c</sup>B<sup>c</sup>) / P(B<sup>c</sup>)   
= 1/2


# Contoh soal

## soal
![soal](soal.jpg)

## jawab
![jawaban](jawaban.jpg)